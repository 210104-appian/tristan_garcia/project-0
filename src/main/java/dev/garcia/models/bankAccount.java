package dev.garcia.models;

public class bankAccount {
	protected double balance;
	protected bankStatus status;
	
	public bankAccount() {
		this.balance = 0;
		this.status = bankStatus.PENDING;
	}
	
	public bankAccount(double balance, bankStatus status) {
		this.balance = balance;
		this.status = status;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public bankStatus getStatus() {
		return status;
	}

	public void setStatus(bankStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "bankAccount: balance = " + balance + ", status = " + status;
	}

}
