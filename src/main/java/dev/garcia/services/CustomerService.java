package dev.garcia.services;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.garcia.daos.BankAccountDao;
import dev.garcia.daos.BankAccountDaoImpl;
import dev.garcia.daos.UserDao;
import dev.garcia.daos.UserDaoImpl;
import dev.garcia.models.bankAccount;

public class CustomerService {
	
	static Scanner scanner = new Scanner(System.in);
	private static Logger Log = Logger.getRootLogger();
	
	public void existingCustomerFlow(String username) {
		String selection;
		do {
		System.out.println("Welcome back! Would you like to (1) View the balance of an account, (2) Withdrawal an amount, or (3) Deposit an amount or (4) Add a bank account to your existing accounts or (5) End the session?");
		selection = scanner.next();
			switch(selection) {
			case "1":
				balanceView(username);
				break;
			case "2":
				withdrawal(username);
				break;
			case "3":
				deposit(username);
				break;
			case "4":
				addAccount(username);
				break;
			case "5":
				System.out.println("Goodbye!");
				break;
			default:
				System.out.println("Invalid input. Please try again.");
				break;
			}
		}while(selection.equals("5") != true);
	}
	
	public void newCustomerFlow(String username, String password) {
		BankAccountDao ba = new BankAccountDaoImpl();
		
		System.out.println("Hello! Our system says you are a new customer! Would you like to apply for a new bank account? (Y/N)");
		String selection = scanner.next();
		if(selection.equals("Y") || selection.equals("y")) {
			System.out.println("What is your preferred starting balance?");
			double startBalance = scanner.nextDouble();
			if(startBalance >= 0) {
				ba.newUser(username, password, startBalance);
				System.out.println("Your account has been created, but it must be accepted by the bank first before you can access it.");
				System.out.println("Please come back again once your account is active!");
				Log.info("New customer account created. Username: " + username + "  Starting Balance: " + startBalance);
			}
		}
		else
			System.out.println("You won't be added to our system! Thank you, come again!");
	}
	
	public void balanceView(String username){
		BankAccountDao ba = new BankAccountDaoImpl();
		
		int selection = showAccounts(username);
		double balanceAmount = ba.balanceView(username, selection);
		System.out.println("Your current balance for this account is $" + balanceAmount);
		
	}
	
	public void withdrawal(String username){
		BankAccountDao ba = new BankAccountDaoImpl();
		
		int selection = showAccounts(username);
		System.out.println("How much would you like to withdrawal?");
		double withdrawalAmount = scanner.nextDouble();
		if(validWithdrawal(ba.balanceView(username, selection), withdrawalAmount) == true) {
			if(ba.getStatus(username,selection).equals("PENDING") == false) {
				ba.withdrawal(username, selection, ba.balanceView(username, selection) - withdrawalAmount);
				System.out.println("Money has been withdrawn successfully.");
				Log.info("Withdrawal transaction. Username: " + username + "  Withdrawal Amount: " + withdrawalAmount);
			}
			else 
				System.out.println("Your account is still pending! No transactions at this time, please wait until your account is active.");
		}
		else 
			System.out.println("Invalid withdrawal! Please try again with a new withdrawal amount.");
	}
	
	public void deposit(String username){
		BankAccountDao ba = new BankAccountDaoImpl();
		
		int selection = showAccounts(username);
		System.out.println("How much would you like to deposit?");
		double depositAmount = scanner.nextDouble();
		if(validDeposit(depositAmount) == true) {
			if(ba.getStatus(username,selection).equals("PENDING") == false) {
				ba.deposit(username, selection, ba.balanceView(username, selection) + depositAmount);
				System.out.println("Money has been deposited successfully.");
				Log.info("Deposit transaction. Username: " + username + "  Deposit Amount: " + depositAmount);
			}
			else
				System.out.println("Your account is still pending! No transactions at this time, please wait until your account is active.");
		}
		else 
			System.out.println("Invalid deposit! Please try again with a new deposit amount.");
	}
	
	public void addAccount(String username) {
		BankAccountDao ba = new BankAccountDaoImpl();
		
		System.out.println("What is your preferred starting balance?");
		double startBalance = scanner.nextDouble();
		if(startBalance >= 0) {
			ba.newAccount(username, startBalance);
			System.out.println("Your account has been created, but it must be accepted by the bank first before you can access it.");
			System.out.println("Please come back again once your account is active!");
			Log.info("New account created for existing customer. Username: " + username + "  Starting Balance: " + startBalance);
		}
		else 
			System.out.println("Invalid deposit! Please try again with a new deposit amount.");
		
	}
	
	public boolean validWithdrawal(double balanceAmount, double withdrawalAmount) {
		if((balanceAmount - withdrawalAmount < 0) || (withdrawalAmount <= 0))
			return false;
		return true;
	}
	public boolean validDeposit(double depositAmount) {
		return (depositAmount > 0);
	}
	
	public int showAccounts(String username) {
		BankAccountDao ba = new BankAccountDaoImpl();
		
		System.out.println("Here are your current accounts!");
		ArrayList<bankAccount> accounts = ba.getAccounts(username);
		for (int i = 0; i < accounts.size(); i++)  
			System.out.print("["+(i+1)+"] " + accounts.get(i) + "\n");  
		System.out.println("Which account would you like to select?");
		int selection = scanner.nextInt();
		return selection;
	}
}
