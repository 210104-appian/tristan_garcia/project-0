package dev.garcia.daos;

import java.util.ArrayList;

import dev.garcia.models.Customer;
import dev.garcia.models.bankAccount;

public interface BankAccountDao {
	public void deposit(String user, int accountSelection, double depositAmount);
	public void withdrawal(String username, int accountSelection, double withdrawalAmount);
	public double balanceView(String username, int accountSelection);
	public void newUser(String username, String password, double balanceAmount);
	public void newAccount(String username, double balanceAmount);
	public String getStatus(String username, int accountSelection);
	public void showAllPendingAccounts();
	public void updatePendingAccounts(String username, String status, int bankID);
	public int accountSize(String username);
	public ArrayList<bankAccount> getAccounts(String username); 

}
