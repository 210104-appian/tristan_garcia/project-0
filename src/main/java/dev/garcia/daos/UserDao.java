package dev.garcia.daos;

public interface UserDao {

	public boolean isExistingCustomer(String username, String password);
	public boolean isNewCustomer(String username, String password);
	public boolean isEmployee(String username, String password);
	
}
